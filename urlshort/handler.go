package urlshort

import "net/http"

func  MapHandler(pathsToUrls map[string]string, fallback http.Handler) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request){
		//give me url's path.
		path := r.URL.Path
		if dest, ok := pathsToUrls[path];ok {
			http.Redirect(w,r,dest,http.StatusFound)
			return
		}
	//if we can match a path..
		//redirect to it
		//else..
		fallback.ServeHTTP(w,r)

}


}
