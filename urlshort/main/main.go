package main

import (
	"fmt"
	"learninggolang/urlshort"
	"net/http"


)

func main(){
	//mux := http.de

	pathsToUrls := map[string]string{
		"/urlshort-godoc": "https://godoc.org/github.com/gophercises/urlshort",
		"/myGit":"https:www.gitlab.com/SMe12435",
	}
	mapHandler := urlshort.MapHandler(pathsToUrls,mux)


	fmt.Println("Starting the server on :8080")
	http.ListenAndServe(":8080",mapHandler)
}