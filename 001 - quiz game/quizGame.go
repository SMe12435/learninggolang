package main

import (
	"encoding/csv"
	"flag"
	"fmt"
	"os"
	"time"
)

func main() {
	csvFilename := flag.String("csv", "problems.csv", "a csv file in the format of question,answer")
	timeLimit := flag.Int("limit",30,"time limit for the quiz in seconds")

	flag.Parse()

	file, err := os.Open(*csvFilename)
	if err != nil {
		exit(fmt.Sprintf("failed opening file: %s\n", *csvFilename))
	}
	r := csv.NewReader(file)
	//here we are not reading line by line and reading all because there is no quiz with billions of lines and would create
	//memory shortage and hence reading all file at one
	lines, err := r.ReadAll()
	if err != nil {
		exit("Failed to parse the csv file")
	}
	problems := parseLines(lines)

	timer := time.NewTimer(time.Duration(*timeLimit) * time.Second)

	//fmt.Println(problems)
	correct := 0

	//problemloop:   ---- this problem loop below is given in line 47
	for i,p := range problems{
		fmt.Printf("Problem #%d: %s = \n",i+1, p.q)
		answerCh := make( chan string)
		go func() {
			var answer string
			fmt.Scanf("%s\n",&answer)
			answerCh <- answer
		}()
		select {
		case <- timer.C:
			fmt.Printf("\nYou scored %d out of %d",correct,len(problems))
			return
			//break problemloop  ---- we can use this problem loop instead of return. because normal break will break the select and not the loop.
		case answer:= <-answerCh:
			//scanf removes all spaces to it wont scan 'the man walked' in the correct way. Here in our quiz the answer is only one word. so it shall work
			//now here scanf will wait forever even if time ended hence we have commented that below
			//fmt.Scanf("%s\n",&answer)
			if answer == p.a {
				correct++
			}
		}
	}
	fmt.Printf("You scored %d out of %d",correct,len(problems))
}
	func parseLines(lines [][]string) []problem{
		ret := make([]problem,len(lines))
		for i, line := range lines {
			ret[i] = problem{
				q: line[0],
				a: line[1],
			}
		}
		return ret
	}

	type problem struct {
		q string
		a string
	}


func exit(msg string){
	fmt.Println(msg)
	os.Exit(1)
}